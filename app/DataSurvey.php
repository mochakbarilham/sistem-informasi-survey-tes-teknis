<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DataSurvey extends Model
{
    protected $table = 'data_survey';
    protected $fillable = [
        'nama', 'nik', 'tanggal_lahir', 'tahun_lahir', 'jenis_kelamin',
        'pendidikan_terakhir', 'pengeluaran_air', 'pengeluaran_listrik',
        'id_surveyor', 'id_moderator', 'data_status'
    ];

    protected $appends = [
        'nama_surveyor' , 'nama_moderator'
    ];

    public function getNamaSurveyorAttribute(){
        return User::find($this->id_surveyor)->name;
    }

    public function getNamaModeratorAttribute(){
        $moderator = User::find($this->id_moderator);
        if ($moderator==NULL) return "-";
        return $moderator->name;
    }
}
