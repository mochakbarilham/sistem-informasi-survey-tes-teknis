<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class LoginController extends Controller
{
    public function index(){
        return view('login');
    }

    public function login(Request $request){
        $account = User::where('username',$request->username)->first();
        if ($account!=null)
        if(Hash::check($request->password, $account->password)){
            $request->session()->put('id',$account->id);
            $request->session()->put('user',$account->username);
            $request->session()->put('name',$account->name);
            $request->session()->put('hak_akses',$account->hak_akses);
            return redirect()->route('dashboard');
        }
        return redirect()->route('home')->withInput()->with('failed','Wrong Username & Password');
    }

    public function logout(Request $request){
        $request->session()->forget('id');
        $request->session()->forget('user');
        $request->session()->forget('name');
        $request->session()->forget('hak_akses');
        return redirect()->route('home')->withInput()->with('logout','Logout');
    }
}
