<?php

namespace App\Http\Controllers;

use App\DataSurvey;
use Illuminate\Http\Request;

class NewSurveyController extends Controller
{
    public function index(Request $request){
        return view('formsurvey');
    }

    public function post(Request $request){
        DataSurvey::create([
            'nama' => $request->nama,
            'nik' => $request->nik,
            'tanggal_lahir' => $request->tanggal_lahir,
            'tahun_lahir' => date("Y",strtotime($request->tanggal_lahir)),
            'jenis_kelamin' => $request->jenis_kelamin,
            'pendidikan_terakhir' => $request->pendidikan_terakhir,
            'pengeluaran_air' => $request->pengeluaran_air,
            'pengeluaran_listrik' =>$request->pengeluaran_listrik,
            'id_surveyor' => $request->session()->get('id'),
            'id_moderator' => $request->id_moderator,
            'data_status' => "Belum Diverifikasi"
        ]);

        return redirect()->back();
    }
}
