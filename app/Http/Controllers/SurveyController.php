<?php

namespace App\Http\Controllers;

use App\DataSurvey;
use Illuminate\Http\Request;

class SurveyController extends Controller
{
    public function index(Request $request){
        return view('tabelsurvey',[
            'dataSurvey' => DataSurvey::all()
        ]);
    }

    public function indexVerifikasi(Request $request){
        return view('verifikasidata',[
            'dataSurvey' => DataSurvey::where('data_status','Belum Diverifikasi')->get()
        ]);
    }

    public function updateVerifikasi(Request $request){
        DataSurvey::find($request->id)->update([
            'data_status' => $request->data_status,
            'id_moderator' => $request->session()->get('id')
        ]);

        return redirect()->back();
    }
}
