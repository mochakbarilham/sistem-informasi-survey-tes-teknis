<?php

namespace App\Http\Controllers;

use App\DataSurvey;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
    public function index(Request $request){
        $tahun = DataSurvey::where('data_status','Diterima')->distinct()->get(['tahun_lahir']);

        if ($tahun->count()!=0){
            if($request->year==null) $tahun_ini = $tahun->first()->tahun_lahir;
            else $tahun_ini = $request->year;

            $data_pertahun[0]['jenjang'] = "SD";
            $data_pertahun[0]['jumlah'] = DataSurvey::where([
                ['tahun_lahir',$tahun_ini],
                ['pendidikan_terakhir',"SD"],
                ['data_status','Diterima']
                ])->count();
            $data_pertahun[1]['jenjang'] = "SMP";
            $data_pertahun[1]['jumlah'] = DataSurvey::where([
                ['tahun_lahir',$tahun_ini],
                ['pendidikan_terakhir',"SMP"],
                ['data_status','Diterima']
                ])->count();
            $data_pertahun[2]['jenjang'] = "SMA";
            $data_pertahun[2]['jumlah'] = DataSurvey::where([
                ['tahun_lahir',$tahun_ini],
                ['pendidikan_terakhir',"SMA"],
                ['data_status','Diterima']
                ])->count();
            $data_pertahun[3]['jenjang'] = "Diploma";
            $data_pertahun[3]['jumlah'] = DataSurvey::where([
                ['tahun_lahir',$tahun_ini],
                ['pendidikan_terakhir',"Diploma"],
                ['data_status','Diterima']
                ])->count();
            $data_pertahun[4]['jenjang'] = "Sarjana";
            $data_pertahun[4]['jumlah'] = DataSurvey::where([
                ['tahun_lahir',$tahun_ini],
                ['pendidikan_terakhir',"Sarjana"],
                ['data_status','Diterima']
                ])->count();
            return view('dashboard',[
                'status' => 'already',
                'tahun' => $tahun,
                'tahun_ini' => $tahun_ini,
                'data_total' => $data_pertahun
            ]);
        }
        return view('dashboard',[
            'status' => 'new'
        ]);
    }
}
