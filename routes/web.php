<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'LoginController@index')->name('home')->middleware('auth.public');
Route::post('/', 'LoginController@login')->name('login')->middleware('auth.public');
Route::get('logout', 'LoginController@logout')->name('logout');

Route::get('/dashboard', 'DashboardController@index')->name('dashboard')->middleware('auth.user');
Route::get('/form', 'NewSurveyController@index')->name('form')->middleware('auth.user');
Route::post('/form', 'NewSurveyController@post')->name('form.post')->middleware('auth.user');

Route::get('/survey','SurveyController@index')->name('survey')->middleware('auth.user');
Route::get('/verifikasi','SurveyController@indexVerifikasi')->name('verifikasi')->middleware('auth.user','auth.moderator');
Route::post('verifikasi','SurveyController@updateVerifikasi')->name('verifikasi.update')->middleware('auth.user','auth.moderator');


