<?php

use App\DataSurvey;
use Illuminate\Database\Seeder;

use Faker\Factory as Faker;

class SurveySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create('id_ID');

        for($nip = 1; $nip <=150; $nip++){
            $dt= $faker->dateTimeBetween($startdate = '-10 years',$endDate = 'now');
            $date= $dt->format("Y-m-d");
            $year=date("Y",strtotime($date));
            DataSurvey::insert([
                'nama' => $faker->name,
                'nik' => $faker->numberBetween(3578272,4578572),
                'tanggal_lahir' => $date,
                'tahun_lahir' => $year,
                'jenis_kelamin' => $faker->randomElement(['Laki-Laki', 'Perempuan']),
                'pendidikan_terakhir' => $faker->randomElement(['SD', 'SMP', 'SMA', 'Diploma', 'Sarjana']),
                'pengeluaran_air' => $faker->numberBetween(10000,200000),
                'pengeluaran_listrik' => $faker->numberBetween(10000,200000),
                'id_surveyor' => 1,
                'id_moderator' => 2,
                'data_status' => $faker ->randomElement(['Diterima','Belum Diverifikasi','Ditolak']),
            ]);
        }
    }
}
