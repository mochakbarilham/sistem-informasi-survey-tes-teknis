<?php

use App\User;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('username')->unique();
            $table->string('password');
            $table->string('hak_akses');
            $table->timestamps();
        });

        User::create([
            'name'=>'Pak Surveyor',
            'username' =>'surveyor1',
            'password' => Hash::make('password123'),
            'hak_akses' => 'surveyor',
        ]);

        User::create([
            'name'=>'Pak Moderator',
            'username' =>'moderator1',
            'password' => Hash::make('password123'),
            'hak_akses' => 'moderator',
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
