<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDataSurvey extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('data_survey', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nama');
            $table->string('nik');
            $table->date('tanggal_lahir');
            $table->unsignedInteger('tahun_lahir');
            $table->string('jenis_kelamin');
            $table->enum('pendidikan_terakhir',[
                'SD','SMP','SMA','Diploma','Sarjana'
            ]);
            $table->unsignedBigInteger('pengeluaran_air')->default(0);
            $table->unsignedBigInteger('pengeluaran_listrik')->default(0);
            $table->unsignedBigInteger('id_surveyor');
            $table->unsignedBigInteger('id_moderator')->nullable(true);
            $table->string('data_status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('data_survey');
    }
}
