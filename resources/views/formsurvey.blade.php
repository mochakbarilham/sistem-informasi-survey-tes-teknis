@extends('layout')
@section('title', 'Welcome')
@section('content')

<!-- Font Awesome -->
<link rel="stylesheet"
    href="{{ asset('AdminLTE/plugins/fontawesome-free/css/all.min.css') }}">
<!-- Ionicons -->
<link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">

<!-- Small boxes (Stat box) -->
<div class="row ">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                <h4 class="card-title">Tambah Data</h4>
            </div>
            <div class="card-body">
                <form action="{{route('form.post')}}" method="POST">
                    @csrf
                    <div class="modal-body">
                        <div class="modal-body">
                            <div class="mr-2 ml-2">
                                <label>Nama</label>
                                <input type="text" class="form-control mb-2" placeholder="" name="nama" required>
                                <label>NIK</label>
                                <input type="number" class="form-control mb-2" placeholder="" name="nik" required>
                                <label>Tanggal Lahir</label>
                                <input type="date" class="form-control mb-2" placeholder="" name="tanggal_lahir" required>
                                <div class='row'>
                                    <div class="col-md-6">
                                        <label>Jenis Kelamin</label>
                                        <select class="custom-select mb-2" name="jenis_kelamin" required>
                                            <option value="" disabled>Choose..</option>
                                            <option value="Laki - Laki">Laki - Laki</option>
                                            <option value="Perempuan">Perempuan</option>
                                        </select>
                                    </div>
                                    <div class="col-md-6">
                                        <label>Pendidikan Terakhir</label>
                                        <select class="custom-select mb-2" name="pendidikan_terakhir" required>
                                            <option value="" disabled>Choose..</option>
                                            <option value="SD">SD</option>
                                            <option value="SMP">SMP</option>
                                            <option value="SMA">SMA</option>
                                            <option value="Diploma">Diploma</option>
                                            <option value="Sarjana">Sarjana</option>
                                        </select>
                                    </div>
                                    <div class="col-md-6">
                                        <label>Pengeluaran Air (Rp.)</label>
                                        <input type="number" class="form-control mb-2" placeholder="" name="pengeluaran_air" value="0" required>
                                    </div>
                                    <div class="col-md-6">
                                        <label>Pengeluaran Listrik (Rp.)</label>
                                        <input type="number" class="form-control mb-2" placeholder="" name="pengeluaran_listrik" value="0" required>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="modal-footer justify-content-right">
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

@endsection

