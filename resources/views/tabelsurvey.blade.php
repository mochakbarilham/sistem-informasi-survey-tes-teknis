@extends('layout')
@section('title', 'Welcome')
@section('content')

<!-- Font Awesome -->
<link rel="stylesheet"
    href="{{ asset('AdminLTE/plugins/fontawesome-free/css/all.min.css') }}">
<!-- Ionicons -->
<link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">

<!-- Small boxes (Stat box) -->
<div class="row ">
    <div class="col-md-12 text-center">

        <table id="tablesurvey" class="table table-bordered table-striped dt-bootstrap4">
            <thead>
                <tr class="text-center align-center">
                    <th rowspan="2">Nama</th>
                    <th rowspan="2">NIK</th>
                    <th rowspan="2">Tanggal Lahir</th>
                    <th rowspan="2">Jenis Kelamim</th>
                    <th rowspan="2">Pendidikan Terakhir</th>
                    <th colspan="2">Pengeluaran</th>
                    <th rowspan="2">Surveyor</th>
                    <th rowspan="2">Moderator</th>
                    <th rowspan="2">Status</th>
                </tr>
                <tr class="text-center">
                    <th>Air</th>
                    <th>Listrik</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($dataSurvey as $data)
                <tr>
                    <td>{{$data->nama}}</td>
                    <td>{{$data->nik}}</td>
                    <td>{{date('d-M-Y', strtotime($data->tanggal_lahir))}}</td>
                    <td>{{$data->jenis_kelamin}}</td>
                    <td>{{$data->pendidikan_terakhir}}</td>
                    <td>{{$data->pengeluaran_air}}</td>
                    <td>{{$data->pengeluaran_listrik}}</td>
                    <td>{{$data->nama_surveyor}}</td>
                    <td>{{$data->nama_moderator}}</td>
                    <td>{{$data->data_status}}</td>

                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>

@endsection

@section('js')
<script>
    $(document).ready(function() {
        $('#tablesurvey').DataTable();
    } );
</script>
@endsection
