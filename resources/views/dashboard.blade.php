@extends('layout')
@section('title', 'Welcome')
@section('content')

<!-- Font Awesome -->
<link rel="stylesheet"
    href="{{ asset('AdminLTE/plugins/fontawesome-free/css/all.min.css') }}">
<!-- Ionicons -->
<link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">

<!-- Small boxes (Stat box) -->
<div class="row ">
    <div class="col-md-12">
        <h3>Selamat Datang, {{Session::get('name')}}</h3>
        @if($status!='new')
        <form class="col-md-12 mt-4" action="{{route('dashboard')}}">
            <div class="row">
                <div class="col-md-12">
                    <label for="year">Persebaran Pendidikan per Tahun</label>
                </div>
                <div class="col-md-11 mb-4">
                    <select class="custom-select" id="year" name="year" required>
                        @foreach($tahun as $data)
                        <option value="{{$data->tahun_lahir}}" @if($data->tahun_lahir==$tahun_ini) selected @endif>{{$data->tahun_lahir}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="col-md-1">
                    <button type="submit" class="btn btn-primary">Cari</button>
                </div>
            </div>
        </form>
        <div class="col-md-12">
            <div class="card">
                <div class="card-header bg-poltera">
                    <h3 class="card-title">Statistik Persebaran Masyarakat {{$tahun_ini}}</h3>
                </div>
                <!-- /.card-header -->
                <div class="card-body">
                    <div class="chart">
                        <canvas id="barChart" style="min-height: 250px; height: 250px; max-height: 250px; max-width: 100%;"></canvas>
                    </div>
                </div>
            </div>
        </div>
        @else
        <div class="col-md-12 text-center mt-5">
            <h5>Statistik belum ada, tambahkan data survey</h2>
        </div>
        @endif
    </div>
</div>

@endsection

@if($status!='new')
@section('js')
<!-- ChartJS -->
<script src="{{ asset('AdminLTE/plugins/chart.js/Chart.min.js') }}"></script>

<script>
    $(function () {
        // Get context with jQuery - using jQuery's .get() method.

        var areaChartData = {
        labels  : [@foreach($data_total as $data) "{{$data['jenjang']}}", @endforeach],
        datasets: [
            {
            label               : 'Persebaran Masyarakat',
            backgroundColor     : 'rgba(60,141,188,0.9)',
            borderColor         : 'rgba(60,141,188,0.8)',
            pointRadius          : false,
            pointColor          : '#3b8bba',
            pointStrokeColor    : 'rgba(60,141,188,1)',
            pointHighlightFill  : '#fff',
            pointHighlightStroke: 'rgba(60,141,188,1)',
            data                : [@foreach($data_total as $data) {{$data['jumlah']}}, @endforeach]
            },
        ]
        }

        var areaChartOptions = {
        maintainAspectRatio : false,
        responsive : true,
        legend: {
            display: false
        },
        scales: {
            xAxes: [{
            gridLines : {
                display : false,
            }
            }],
            yAxes: [{
            gridLines : {
                display : false,
            }
            }]
        }
        }
        //-------------
        //- BAR CHART -
        //-------------
        var barChartCanvas = $('#barChart').get(0).getContext('2d')
        var barChartData = jQuery.extend(true, {}, areaChartData)
        var temp0 = areaChartData.datasets[0]
        barChartData.datasets[0] = temp0

        var barChartOptions = {
        responsive              : true,
        maintainAspectRatio     : false,
        datasetFill             : false
        }

        var barChart = new Chart(barChartCanvas, {
        type: 'bar',
        data: barChartData,
        options: barChartOptions
        })


    })
</script>

@endsection
@endif

