@extends('layout')
@section('title', 'Welcome')
@section('content')

<!-- Font Awesome -->
<link rel="stylesheet"
    href="{{ asset('AdminLTE/plugins/fontawesome-free/css/all.min.css') }}">
<!-- Ionicons -->
<link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">

<!-- Small boxes (Stat box) -->
<div class="row">
    <div class="col-md-12 text-center">
        <table id="tablesurvey" class="table table-bordered table-striped dt-bootstrap4">
            <thead>
                <tr class="text-center align-center">
                    <th rowspan="2">Nama</th>
                    <th rowspan="2">NIK</th>
                    <th rowspan="2">Tanggal Lahir</th>
                    <th rowspan="2">Jenis Kelamim</th>
                    <th rowspan="2">Pendidikan Terakhir</th>
                    <th colspan="2">Pengeluaran</th>
                    <th rowspan="2">Surveyor</th>
                    <th rowspan="2">Status</th>
                </tr>
                <tr class="text-center">
                    <th>Air</th>
                    <th>Listrik</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($dataSurvey as $data)
                <tr>
                    <td>{{$data->nama}}</td>
                    <td>{{$data->nik}}</td>
                    <td>{{date('d-M-Y', strtotime($data->tanggal_lahir))}}</td>
                    <td>{{$data->jenis_kelamin}}</td>
                    <td>{{$data->pendidikan_terakhir}}</td>
                    <td>{{$data->pengeluaran_air}}</td>
                    <td>{{$data->pengeluaran_listrik}}</td>
                    <td>{{$data->nama_surveyor}}</td>
                    <td class="text-center">
                        <div class="row">
                            <div class="col-md-6">
                                <button type="submit" class="btn-xs btn-primary" data-toggle="modal"
                                data-target="#modal-terima{{$data->id}}" data-dismiss="modal">Terima</button>
                            </div>
                            <div class="col-md-6">
                                <button type="submit" class="btn-xs btn-danger" data-toggle="modal"
                                data-target="#modal-tolak{{$data->id}}" data-dismiss="modal">Tolak</button>
                            </div>
                        </div>
                    </td>

                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>

@foreach($dataSurvey as $data)
<div class="modal fade" id="modal-terima{{$data->id}}">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Terima Survey</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body text-center">
                <div class="row">
                    <div class="col-sm-2 icon">
                        <i class="fa fa-check fa-2x"></i>
                    </div>
                    <div class="col-sm-10">
                        <h5>Anda yakin ingin menerima survey pada <b>{{$data->nama}}</b> ?</h5>
                    </div>
                </div>
            </div>
            <form action="{{route('verifikasi.update')}}" method="POST">
                @csrf
                <input type="hidden" name="id" value="{{$data->id}}">
                <input type="hidden" name="data_status" value="Diterima">
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                    <button type="submit" class="btn btn-primary">Terima</button>
                </div>
            </form>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->

<div class="modal fade" id="modal-tolak{{$data->id}}">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Tolak Survey</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body text-center">
                <div class="row">
                    <div class="col-sm-2 icon">
                        <i class="fa fa-trash fa-2x text-danger"></i>
                    </div>
                    <div class="col-sm-10">
                        <h5>Anda yakin ingin menolak survey pada <b>{{$data->nama}}</b> ?</h5>
                    </div>
                </div>
            </div>
            <form action="{{route('verifikasi.update')}}" method="POST">
                @csrf
                <input type="hidden" name="id" value="{{$data->id}}">
                <input type="hidden" name="data_status" value="Ditolak">
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                    <button type="submit" class="btn btn-danger">Tolak</button>
                </div>
            </form>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->
@endforeach

@endsection

@section('js')
<script>
    $(document).ready(function() {
        $('#tablesurvey').DataTable();
    } );
</script>
@endsection
