<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="x-ua-compatible" content="ie=edge">

    <title>@yield('title')</title>
    <link rel="shortcut icon" href="{{asset('/assets/Logo.png') }}" />

    <!-- Font Awesome Icons -->
    <link rel="stylesheet"
        href="{{asset('AdminLTE/plugins/fontawesome-free/css/all.min.css') }}">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{asset('AdminLTE/dist/css/adminlte.min.css') }}">
    <!-- Google Font: Source Sans Pro -->
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">

    <!-- DataTables -->
    <link rel="stylesheet" href="{{asset('AdminLTE/plugins/datatables-bs4/css/dataTables.bootstrap4.css')}}">

</head>

<body class="hold-transition layout-top-nav">
    <div class="wrapper">

        <!-- Navbar -->
        <style>
            .navbar-brand {width: 250px !important}
        </style>
        <nav class="main-header navbar navbar-expand-md navbar-dark navbar-olive">
            <div class="container">
                <a href="" class="navbar-brand">
                    <div class="row ">
                        <div class="col-sm-3">
                            <img src="{{asset('/assets/Logo.png') }}" alt="AdminLTE Logo"
                                class="img-circle elevation-3" style="opacity: .8" width="50" height="50">
                        </div>
                        <div class="col-sm-9">
                            <span class="brand-text font-weight-light">Sistem Informasi</span><br>
                            <div class="brand-text font-weight-light" style="font-size:15px">Survey Masyarakat
                            </div>
                        </div>
                    </div>
                </a>


                @if(Session::has('user'))
                {{-- on Session item --}}
                <!-- Left navbar links -->
                <ul class="navbar-nav">
                    <li class="nav-item">
                        <a href="{{route('dashboard')}}" class="nav-link">Dashboard</a>
                    </li>
                    @if(Session::get('hak_akses')=='moderator')
                    <li class="nav-item">
                        <a href="{{route('verifikasi')}}" class="nav-link">Verifikasi Survey</a>
                    </li>
                    @endif
                    <li class="nav-item dropdown">
                        <a id="dropdownSubMenu1" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="nav-link dropdown-toggle">Survey</a>
                        <ul aria-labelledby="dropdownSubMenu1" class="dropdown-menu border-0 shadow">
                        <li><a href="{{route('survey')}}" class="dropdown-item">Lihat Survey </a></li>
                        <li><a href="{{route('form')}}" class="dropdown-item">Tambah Survey</a></li>
                        </ul>
                    </li>
                </ul>
                <!-- Right navbar links -->
                <ul class="order-1 order-md-3 navbar-nav navbar-no-expand ml-auto">
                    <li class="nav-item">
                        <a class="nav-link" href="{{route('logout')}}">Logout</a>
                    </li>

                </ul>
                {{-- /.on Session item --}}
                @endif

            </div>
        </nav>
        <!-- /.navbar -->

        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <div class="content-header">
                <div class="container">
                    <div class="row mt-2 mb-2">
                        <div class="col-sm-10">
                            <h1 class="m-0 text-dark">@yield('header')</h1>
                        </div><!-- /.col -->
                    </div><!-- /.row -->
                </div><!-- /.container-fluid -->
            </div>
            <!-- /.content-header -->

            <!-- Main content -->
            <div class="content">
                <div class="container">
                    @yield('content')
                </div><!-- /.container-fluid -->
            </div>
            <!-- /.content -->
        </div>
        <!-- /.content-wrapper -->

        {{-- <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
    <div class="p-3">
      <h5>Title</h5>
      <p>Sidebar content</p>
    </div>
  </aside>
  <!-- /.control-sidebar --> --}}

        <!-- Main Footer -->
        <footer class="main-footer bg-dark bg-olive">
            <!-- To the right -->
            <div class="float-right d-none d-sm-inline">
                Anything you want
            </div>
            <!-- Default to the left -->
            <strong>Copyright &copy; 2014-2019 <a href="https://adminlte.io">AdminLTE.io</a>.</strong> All rights
            reserved.
        </footer>
    </div>
    <!-- ./wrapper -->

    <!-- REQUIRED SCRIPTS -->

    <!-- jQuery -->
    <script src="{{asset('AdminLTE/plugins/jquery/jquery.min.js') }}"></script>
    <!-- Bootstrap 4 -->
    <script src="{{asset('AdminLTE/plugins/bootstrap/js/bootstrap.bundle.min.js') }}">
    </script>
    <!-- AdminLTE App -->
    <script src="{{asset('AdminLTE/dist/js/adminlte.min.js') }}"></script>

    <!-- DataTables -->
    <script src="{{asset('AdminLTE/plugins/datatables/jquery.dataTables.js')}}"></script>
    <script src="{{asset('AdminLTE/plugins/datatables-bs4/js/dataTables.bootstrap4.js')}}"></script>

    {{-- Ionicons --}}
    <script src="https://unpkg.com/ionicons@5.0.0/dist/ionicons.js"></script>

    @yield('js')
</body>

</html>
